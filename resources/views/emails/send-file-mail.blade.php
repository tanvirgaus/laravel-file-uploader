@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => url('/download/'.  $data->id)])
Click here to download
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
