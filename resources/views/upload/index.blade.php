@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">File Lists</div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sender</th>
                                <th>Receiver</th>
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($files))
                                @foreach($files as $file)
                                    <tr>
                                        <td>
                                            {{ ($file->sender == $user->email) ? 'Own' : $file->sender }}
                                        </td>
                                        <td>
                                            {{ ($file->receiver == $user->email) ? 'Own' : $file->receiver }}
                                        </td>
                                        <td>
                                            <a href="{{ route('download-file', ['fileId' => $file->id]) }}">
                                                Download
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="3">No Record Found!</td>
                                </tr>
                            @endif
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
