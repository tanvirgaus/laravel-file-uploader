<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upload;
use App\Mail\SendFileMail;
class FileUploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
    	return view('upload-file');
    }

    public function store(Request $request) {
    	/**
         * Validate
         */
    	$this->validate($request, [
    		'sender_email' => 'required|email|exists:users,email',
    		'receiver_email' => 'required|email|exists:users,email',
    		'file' => 'required',
    	]);
        /**
         * Get the file
         * then generate a filename with timestamp and
         * Save it in the file system
         */
    	$file = $request->file('file');
    	if($file) {
	    	$name = $file->getClientOriginalName(); // getting original name
	    	$ext = $file->getClientOriginalExtension();
		    $filename = time().'.'.$ext;

		    $destinationPath = public_path('/files');

		    $path = '/files/'.$filename;

		    $data['sender'] = $request->sender_email;
		    $data['receiver'] = $request->receiver_email;
		    $data['file_path'] = $path;
		    $data['ext'] = $ext;

		    $upload = Upload::create($data); // save the info

		    if($upload) {
                \Mail::to($data['sender'])->send(new SendFileMail($upload));
		    	$file->move($destinationPath, $filename);
		    }
		    session()->flash('message', 'Uploaded successfully.');
		    return back();
    	}
    }

    public function downloadFile($fileId) {
    	$file = Upload::findOrFail($fileId);
    	$user = auth()->user();

    	if(!$file) {
    		session()->flash('message', 'File not found!');
    		return redirect('/home');
    	}

    	if($file->sender == $user->email || $file->receiver == $user->email) {
	    	$filePath = public_path($file->file_path);

	    	return response()->download($filePath);
    	}

		session()->flash('message', 'Permission denied!');
		return redirect('/home');
    }

    public function getUploadedFiles() {
    	$user = auth()->user();
    	$files = Upload::where('sender', $user->email)->orWhere('receiver', $user->email)->orderBy('created_at', 'desc')->get();
    	return view('upload.index', compact('files', 'user'));
    }
}
